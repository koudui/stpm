package stpmproject.service;

import stpmproject.error.BusinessException;
import stpmproject.service.model.UserModel;

import java.util.List;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/4/25 19:55
 * @software: IntelliJ IDEA
 * @file: UserService
 * @description:
 */
public interface UserService {
    //根据用户id查找用户
    UserModel getUserById(Integer id);

    //返回用户列表
    List<UserModel> listUser();
    List<UserModel> listProgramer();
    List<UserModel> listDeveloper();
    List<UserModel> listTester();

    //根据前端的信息组成一个Usermodel完成用户的注册
    void register(UserModel userModel) throws BusinessException;

    //根据账号密码判断登录是否成功并返回一个UserModel
    UserModel validateLogin(String userName,String encrptPassword) throws BusinessException;

    UserModel getUserByTelphone(String telphone);

    UserModel getUserByUserName(String userName);

    boolean updateUser(UserModel userModel) throws BusinessException;

    boolean deleteUser(UserModel userModel) throws BusinessException;
}
