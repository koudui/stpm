package stpmproject.service;

import stpmproject.error.BusinessException;
import stpmproject.service.model.TodoModel;

import java.util.List;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 23:51
 * @software: IntelliJ IDEA
 * @file: TodoService
 * @description:
 */
public interface TodoService {
    TodoModel getTodoById(Integer todoId);

    TodoModel getTodoByName(String todoName);

    List<TodoModel> getTodoByCreater(String creater) throws BusinessException;

    List<TodoModel> listTodo();

    void createTodo(TodoModel todoModel) throws BusinessException;

    void updateTodo(TodoModel todoModel) throws BusinessException;

    void deleteTodo(Integer todoId) throws BusinessException;
}
