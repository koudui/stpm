package stpmproject.service;

import stpmproject.error.BusinessException;
import stpmproject.service.model.TeamModel;

import java.util.List;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/23 16:58
 * @software: IntelliJ IDEA
 * @file: TeamService
 * @description:
 */
public interface TeamService {
    TeamModel getTeamById(Integer teamId);

    TeamModel getTeamByProjectCode(String projectCode);

    List<TeamModel> listTeam();

    void createTeam(TeamModel teamModel) throws BusinessException;

    boolean updateTeam(TeamModel teamModel);

    TeamModel getTeamByProjectAndName(String teamProject,String teamName) throws BusinessException;
}
