package stpmproject.service.impl;

import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import stpmproject.dao.ProjectDOMapper;
import stpmproject.dataobject.ProjectDO;
import stpmproject.error.BusinessException;
import stpmproject.error.EmBusinessError;
import stpmproject.service.ProjectService;
import stpmproject.service.model.ProjectModel;
import stpmproject.validator.ValidatorImpl;
import stpmproject.validator.ValidatorResult;

import java.util.List;
import java.util.stream.Collectors;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/21 19:46
 * @software: IntelliJ IDEA
 * @file: ProjectSercviceImpl
 * @description:
 */
@Service
public class ProjectSercviceImpl implements ProjectService {
    @Autowired
    private ProjectDOMapper projectDOMapper;

    @Autowired
    private ValidatorImpl validator;

    //根据id查找项目
    @Override
    public ProjectModel getProjectById(Integer id) {
       ProjectDO projectDO=projectDOMapper.selectByPrimaryKey(id);
       if(projectDO==null){
           return null;
       }
        return convertModelFromDao(projectDO);
    }

    @Override
    public ProjectModel getProjectByCode(String projectCode) {
        ProjectDO projectDO=projectDOMapper.selectByProjectCode(projectCode);
        if(projectCode==null){
            return null;
        }
        return convertModelFromDao(projectDO);
    }

    @Override
    public List<ProjectModel> listProject() {
        List<ProjectDO> projectDOList=projectDOMapper.listProject();
        List<ProjectModel> projectModelList=projectDOList.stream().map(projectDO -> {
            ProjectModel projectModel=this.convertModelFromDao(projectDO);
            return projectModel;
        }).collect(Collectors.toList());
        return projectModelList;
    }

    @Override
    public void createProject(ProjectModel projectModel) throws BusinessException {
        if(projectModel==null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        ValidatorResult result=validator.validate(projectModel);
        if(result.getHasErr()==true){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,result.getErrMsg());
        }
        ProjectDO projectDO=convertDaoFromModel(projectModel);
        try{
            projectDOMapper.insertSelective(projectDO);
        }catch (DuplicateKeyException e){
            e.printStackTrace();
            throw new BusinessException(EmBusinessError.PROJECT_EXIST);
        }
        return;
    }

    @Override
    public boolean deleteProject(Integer projectId) {
        int affectRows=projectDOMapper.deleteByPrimaryKey(projectId);
        if(affectRows>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean updateProject(ProjectModel projectModel) throws BusinessException {
        if(projectModel==null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        ValidatorResult result=validator.validate(projectModel);
        if(result.getHasErr()==true){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,result.getErrMsg());
        }
        ProjectDO projectDO=convertDaoFromModel(projectModel);
        int affectRows=projectDOMapper.updateByPrimaryKeySelective(projectDO);
        if(affectRows>0){
            return true;
        }
        return false;
    }

    private ProjectModel convertModelFromDao(ProjectDO projectDO){
        if(projectDO==null){
            return null;
        }
        ProjectModel projectModel=new ProjectModel();
        BeanUtils.copyProperties(projectDO,projectModel);
        //将数据库中时间戳用joda处理
        projectModel.setProjectCreateDate(new DateTime(projectDO.getProjectCreateDate()));
        projectModel.setProjectEndDate(new DateTime(projectDO.getProjectEndDate()));
        projectModel.setProjectStartDate(new DateTime(projectDO.getProjectStartDate()));
        return projectModel;
    }
    private ProjectDO convertDaoFromModel(ProjectModel projectModel){
        if(projectModel==null){
            return null;
        }
        ProjectDO projectDO=new ProjectDO();
        BeanUtils.copyProperties(projectModel,projectDO);
        projectDO.setProjectStartDate(projectModel.getProjectStartDate().toDate());
        projectDO.setProjectEndDate(projectModel.getProjectEndDate().toDate());
        return projectDO;
    }
}
