package stpmproject.service.impl;

import com.sun.xml.internal.bind.v2.TODO;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stpmproject.dao.TodoDOMapper;
import stpmproject.dataobject.TodoDO;
import stpmproject.error.BusinessException;
import stpmproject.error.EmBusinessError;
import stpmproject.service.TodoService;
import stpmproject.service.model.TodoModel;
import stpmproject.validator.ValidatorImpl;
import stpmproject.validator.ValidatorResult;

import java.util.List;
import java.util.stream.Collectors;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 23:55
 * @software: IntelliJ IDEA
 * @file: TodoServiceImpl
 * @description:
 */
@Service
public class TodoServiceImpl implements TodoService {
    @Autowired
    private ValidatorImpl validator;

    @Autowired
    private TodoDOMapper todoDOMapper;

    @Override
    public TodoModel getTodoById(Integer todoId) {
        TodoDO todoDO=todoDOMapper.selectByPrimaryKey(todoId);
        if(todoDO==null){
            return null;
        }
        return convertModelFromDO(todoDO);
    }

    @Override
    public TodoModel getTodoByName(String todoName) {
        TodoDO todoDO=todoDOMapper.selectByName(todoName);
        return convertModelFromDO(todoDO);
    }

    @Override
    public List<TodoModel> getTodoByCreater(String creater) throws BusinessException {
        List<TodoDO> todoDOList=todoDOMapper.selectByCreater(creater);
        if(todoDOList==null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        List<TodoModel> todoModelList=todoDOList.stream().map(todoDO ->{
            TodoModel todoModel=this.convertModelFromDO(todoDO);
            return todoModel;
        } ).collect(Collectors.toList());
        return todoModelList;
    }

    @Override
    public List<TodoModel> listTodo() {
        List<TodoDO> todoDOList=todoDOMapper.listTodo();
        List<TodoModel> todoModelList=todoDOList.stream().map(todoDO ->{
            TodoModel todoModel=this.convertModelFromDO(todoDO);
            return todoModel;
        } ).collect(Collectors.toList());
        return todoModelList;
    }

    @Override
    public void createTodo(TodoModel todoModel) throws BusinessException {
        if(todoModel==null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        ValidatorResult result=validator.validate(todoModel);
        if(result.getHasErr()==true){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,result.getErrMsg());
        }
        TodoDO todoDO=convertDOFromModel(todoModel);
        int affectRow=todoDOMapper.insertSelective(todoDO);
        if(affectRow<0){
            throw new BusinessException(EmBusinessError.UPDATE_FAIL);
        }
        return;
    }

    @Override
    public void updateTodo(TodoModel todoModel) throws BusinessException {
        if(todoModel==null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        TodoDO todoDO=convertDOFromModel(todoModel);
        int affeceRow=todoDOMapper.updateByPrimaryKeySelective(todoDO);
        if(affeceRow<0){
            throw new BusinessException(EmBusinessError.UPDATE_FAIL);
        }
        return;
    }

    @Override
    public void deleteTodo(Integer todoId) throws BusinessException {
        int affecrRow=todoDOMapper.deleteByPrimaryKey(todoId);
        if(affecrRow<0){
            throw new BusinessException(EmBusinessError.DELETA_FAIL);
        }
        return;
    }
    private TodoDO convertDOFromModel(TodoModel todoModel){
        if(todoModel==null){
            return null;
        }
        TodoDO todoDO=new TodoDO();
        BeanUtils.copyProperties(todoModel,todoDO);
        todoDO.setTodoCreateDate(todoModel.getTodoCreateDate().toDate());
        todoDO.setTodoStartDate(todoModel.getTodoStartDate().toDate());
        todoDO.setTodoEndDate(todoModel.getTodoEndDate().toDate());
        return todoDO;
    }
    private TodoModel convertModelFromDO(TodoDO todoDO){
        if(todoDO==null){
            return null;
        }
        TodoModel todoModel=new TodoModel();
        BeanUtils.copyProperties(todoDO,todoModel);
        todoModel.setTodoCreateDate(new DateTime(todoDO.getTodoCreateDate()));
        todoModel.setTodoEndDate(new DateTime(todoDO.getTodoEndDate()));
        todoModel.setTodoStartDate(new DateTime(todoDO.getTodoStartDate()));
        return todoModel;
    }
}
