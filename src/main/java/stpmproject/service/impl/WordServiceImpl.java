package stpmproject.service.impl;

import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stpmproject.dao.WordDOMapper;
import stpmproject.dataobject.WordDO;
import stpmproject.error.BusinessException;
import stpmproject.error.EmBusinessError;
import stpmproject.service.WordService;
import stpmproject.service.model.WordModel;
import stpmproject.validator.ValidatorImpl;
import stpmproject.validator.ValidatorResult;

import java.util.List;
import java.util.stream.Collectors;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 20:35
 * @software: IntelliJ IDEA
 * @file: WordServiceImpl
 * @description:
 */
@Service
public class WordServiceImpl implements WordService {
    @Autowired
    private ValidatorImpl validator;

    @Autowired
    private WordDOMapper wordDOMapper;

    @Override
    public WordModel getWordById(Integer wordId) throws BusinessException {
        WordDO wordDO=wordDOMapper.selectByPrimaryKey(wordId);
        if(wordDO==null){
            throw new BusinessException(EmBusinessError.WORD_NOT_EXIST);
        }
        return convertModelFromDO(wordDO);
    }

    @Override
    public WordModel getWordByTitle(String wordTitle){
        WordDO wordDO=wordDOMapper.selectByTitle(wordTitle);
        if(wordDO==null){
            return null;
        }
        return convertModelFromDO(wordDO);
    }

    @Override
    public List<WordModel> listWord() {
        List<WordDO> wordDOList=wordDOMapper.listWord();
        List<WordModel> wordModelList=wordDOList.stream().map(wordDO -> {
            WordModel wordModel=this.convertModelFromDO(wordDO);
            return wordModel;
        }).collect(Collectors.toList());
        return wordModelList;
    }

    @Override
    public void createWord(WordModel wordModel) throws BusinessException {
        if(wordModel==null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        ValidatorResult result=validator.validate(wordModel);
        if(result.getHasErr()==true){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,result.getErrMsg());
        }
        WordDO wordDO=convertDOFromModel(wordModel);
        wordDOMapper.insertSelective(wordDO);
        return;
    }

    @Override
    public void updateWord(WordModel wordModel) throws BusinessException {
        if(wordModel==null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        WordDO wordDO=convertDOFromModel(wordModel);
        int affectRwo=wordDOMapper.updateByPrimaryKeySelective(wordDO);
        if(affectRwo<0){
            throw new BusinessException(EmBusinessError.DELETA_FAIL);
        }
        return;
    }

    @Override
    public void deleteWord(Integer wordId) throws BusinessException {
        int affectRow=wordDOMapper.deleteByPrimaryKey(wordId);
        if(affectRow<0){
            throw new BusinessException(EmBusinessError.DELETA_FAIL);
        }
        return;
    }
    private WordDO convertDOFromModel(WordModel wordModel){
        if(wordModel==null){
            return null;
        }
        WordDO wordDO=new WordDO();
        BeanUtils.copyProperties(wordModel,wordDO);
        wordDO.setWordCreateDate(wordModel.getWordCreateDate().toDate());
        wordDO.setWordEditDate(wordModel.getWordEditDate().toDate());
        return wordDO;
    }
    private WordModel convertModelFromDO(WordDO wordDO){
        if(wordDO==null){
            return null;
        }
        WordModel wordModel=new WordModel();
        BeanUtils.copyProperties(wordDO,wordModel);
        wordModel.setWordCreateDate(new DateTime(wordDO.getWordCreateDate()));
        wordModel.setWordEditDate(new DateTime(wordDO.getWordEditDate()));
        return wordModel;
    }
}
