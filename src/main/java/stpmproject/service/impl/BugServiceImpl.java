package stpmproject.service.impl;

import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import stpmproject.dao.BugDOMapper;
import stpmproject.dataobject.BugDO;
import stpmproject.error.BusinessException;
import stpmproject.error.EmBusinessError;
import stpmproject.service.BugService;
import stpmproject.service.model.BugModel;
import stpmproject.validator.ValidatorImpl;
import stpmproject.validator.ValidatorResult;

import java.util.List;
import java.util.stream.Collectors;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 13:39
 * @software: IntelliJ IDEA
 * @file: BugServiceImpl
 * @description:
 */
@Service
public class BugServiceImpl implements BugService {
    @Autowired
    private ValidatorImpl validator;

    @Autowired
    private BugDOMapper bugDOMapper;

    @Override
    public BugModel getBugById(Integer bugId) {
        BugDO bugDO=bugDOMapper.selectByPrimaryKey(bugId);
        if(bugDO==null){
            return null;
        }
        return convertModelFromDO(bugDO);
    }

    @Override
    public List<BugModel> listBug() {
        List<BugDO> bugDOList=bugDOMapper.listBug();
        List<BugModel> bugModelList=bugDOList.stream().map(bugDO -> {
            BugModel bugModel=this.convertModelFromDO(bugDO);
            return bugModel;
        }).collect(Collectors.toList());
        return bugModelList;
    }

    @Override
    public List<BugModel> listMyBug(String assginedTo) {
        List<BugDO> bugDOList=bugDOMapper.selectByAssgined("%"+assginedTo+"%");
        List<BugModel> bugModelList=bugDOList.stream().map(bugDO -> {
            BugModel bugModel=this.convertModelFromDO(bugDO);
            return bugModel;
        }).collect(Collectors.toList());
        return bugModelList;
    }

    @Override
    public List<BugModel> groupBugByUserName(String userName) {
        List<BugDO> bugDOList=bugDOMapper.groupByAssgined("%"+userName+"%");
        List<BugModel> bugModelList=bugDOList.stream().map(bugDO -> {
            BugModel bugModel=this.convertModelFromDO(bugDO);
            System.out.println("bugDO.getNumOfProject() = " + bugDO.getNumOfProject());
            return bugModel;
        }).collect(Collectors.toList());
        return bugModelList;
    }

    @Override
    public void createBug(BugModel bugModel) throws BusinessException {
        if(bugModel==null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        ValidatorResult result=validator.validate(bugModel);
        if(result.getHasErr()==true){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,result.getErrMsg());
        }
        BugDO bugDO=convertDOFromModel(bugModel);
        try{
            bugDOMapper.insertSelective(bugDO);
        }catch (DuplicateKeyException e){
            e.printStackTrace();
            throw new BusinessException(EmBusinessError.BUG_EXIST);
        }
        return;
    }

    @Override
    public void updateBug(BugModel bugModel) throws BusinessException {
        if(bugModel==null){
            throw new BusinessException(EmBusinessError.BUG_NOT_EXIST);
        }
        ValidatorResult result=validator.validate(bugModel);
        if(result.getHasErr()==true){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,result.getErrMsg());
        }
        BugDO bugDO=convertDOFromModel(bugModel);
        bugDOMapper.updateByPrimaryKeySelective(bugDO);
        return;

    }

    @Override
    public void deleteBugById(Integer bugId) throws BusinessException {
        int affectRow=bugDOMapper.deleteByPrimaryKey(bugId);
        if(affectRow<0){
            throw new BusinessException(EmBusinessError.DELETA_FAIL);
        }
        return;
    }

    @Override
    public BugModel getBugByNameAndProjectCode(String bugName,String bugProjectCode,String assginedTo) {
        BugDO bugDO=bugDOMapper.selectByNameAndProjctCode(bugName,bugProjectCode,assginedTo);
        if(bugDO==null){
            return null;
        }
        return convertModelFromDO(bugDO);
    }

    private BugDO convertDOFromModel(BugModel bugModel){
        if(bugModel==null){
            return null;
        }
        BugDO bugDO=new BugDO();
        BeanUtils.copyProperties(bugModel,bugDO);
        bugDO.setBugStartDate(bugModel.getBugStartDate().toDate());
        bugDO.setBugEndDate(bugModel.getBugEndDate().toDate());
        bugDO.setBugCreateDate(bugModel.getBugCreateDate().toDate());
        return bugDO;
    }

    private BugModel convertModelFromDO(BugDO bugDO){
        if(bugDO==null){
            return null;
        }
        BugModel bugModel=new BugModel();
        BeanUtils.copyProperties(bugDO,bugModel);
        bugModel.setBugStartDate(new DateTime(bugDO.getBugStartDate()));
        bugModel.setBugEndDate(new DateTime(bugDO.getBugEndDate()));
        bugModel.setBugCreateDate(new DateTime(bugDO.getBugCreateDate()));
        return bugModel;
    }
}
