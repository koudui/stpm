package stpmproject.service.impl;

import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stpmproject.dao.TaskDOMapper;
import stpmproject.dataobject.TaskDO;
import stpmproject.error.BusinessException;
import stpmproject.error.EmBusinessError;
import stpmproject.service.TaskService;
import stpmproject.service.model.ProjectModel;
import stpmproject.service.model.TaskModel;
import stpmproject.validator.ValidatorImpl;
import stpmproject.validator.ValidatorResult;

import java.util.List;
import java.util.stream.Collectors;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/25 23:09
 * @software: IntelliJ IDEA
 * @file: TaskServiceImpl
 * @description:
 */
@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    private ValidatorImpl validator;

    @Autowired
    private TaskDOMapper taskDOMapper;

    @Override
    public TaskModel getTaskById(Integer taskId) {
        TaskDO taskDO=taskDOMapper.selectByPrimaryKey(taskId);
        if(taskDO==null){
            return null;
        }
        return convertModelFromDao(taskDO);
    }

    @Override
    public List<ProjectModel> getTaskByAssgined(String userName) {
        List<TaskDO> taskDOList=taskDOMapper.selectByAssgined("%"+userName+"%");
        List<ProjectModel> projectModelList=taskDOList.stream().map(taskDO -> {
            TaskModel taskModel=this.convertModelFromDao(taskDO);
            ProjectModel projectModel=new ProjectModel();
            projectModel.setProjectCode(taskModel.getTaskProjectCode());
            return projectModel;
        }).collect(Collectors.toList());
        return projectModelList;
    }

    @Override
    public List<TaskModel> getTaskByAssginedTo(String userName) {
        List<TaskDO> taskDOList=taskDOMapper.selectByAssgined(userName);
        List<TaskModel> taskModelList=taskDOList.stream().map(taskDO -> {
            TaskModel taskModel=this.convertModelFromDao(taskDO);
            return taskModel;
        }).collect(Collectors.toList());
        return taskModelList;
    }

    @Override
    public TaskModel getByProjectAndNameAndTeam(String project, String name, String team) throws BusinessException {
        TaskDO taskDO=taskDOMapper.selectByProjectAndNameAndTeam(project,name,team);
        if(taskDO!=null){
            throw new BusinessException(EmBusinessError.TASK_EXIST);
        }
        return null;
    }

    @Override
    public List<TaskModel> listTask() {
        List<TaskDO> taskDOList=taskDOMapper.listTask();
        List<TaskModel> taskModelList=taskDOList.stream().map(taskDO -> {
            TaskModel taskModel=this.convertModelFromDao(taskDO);
            return taskModel;
        }).collect(Collectors.toList());
        return taskModelList;
    }

    @Override
    public List<TaskModel> groupByUserName(String userName) {
        List<TaskDO> taskDOList=taskDOMapper.groupByAssgined("%"+userName+"%");
        List<TaskModel> taskModelList=taskDOList.stream().map(taskDO -> {
            TaskModel taskModel=this.convertModelFromDao(taskDO);
            System.out.println("taskModel.getNumOfProject() = " + taskModel.getNumOfProject());
            return taskModel;
        }).collect(Collectors.toList());
        return taskModelList;
    }

    @Override
    public void createTask(TaskModel taskModel) throws BusinessException {
        if(taskModel==null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        ValidatorResult result=validator.validate(taskModel);
        if(result.getHasErr()==true){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,result.getErrMsg());
        }
        TaskDO taskDO=convertDoFromModel(taskModel);
        int affectRow=taskDOMapper.insertSelective(taskDO);
        return;
    }

    @Override
    public void updateTask(TaskModel taskModel) throws BusinessException {
        if(taskModel==null){
            throw new BusinessException(EmBusinessError.TASK_NOT_EXIST);
        }
        TaskDO taskDO=convertDoFromModel(taskModel);
        int affectRow=taskDOMapper.updateByPrimaryKeySelective(taskDO);
        if(affectRow<0){
            throw new BusinessException(EmBusinessError.UPDATE_FAIL);
        }
        return;
    }

    @Override
    public void deleteTaskById(Integer taskId) throws BusinessException {
        int affectRow=taskDOMapper.deleteByPrimaryKey(taskId);
        if(affectRow<0){
            throw new BusinessException(EmBusinessError.DELETA_FAIL);
        }
    }

    private TaskModel convertModelFromDao(TaskDO taskDO){
        if(taskDO==null){
            return null;
        }
        TaskModel taskModel=new TaskModel();
        BeanUtils.copyProperties(taskDO,taskModel);
        taskModel.setTaskStartDate(new DateTime(taskDO.getTaskStartDate()));
        taskModel.setTaskEndDate(new DateTime(taskDO.getTaskEndDate()));
        return taskModel;
    }
    private TaskDO convertDoFromModel(TaskModel taskModel){
        if(taskModel==null){
            return  null;
        }
        TaskDO taskDO=new TaskDO();
        BeanUtils.copyProperties(taskModel,taskDO);
        taskDO.setTaskStartDate(taskModel.getTaskStartDate().toDate());
        taskDO.setTaskEndDate(taskModel.getTaskEndDate().toDate());
        return taskDO;
    }
}
