package stpmproject.service.impl;

import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import stpmproject.dao.TeamDOMapper;
import stpmproject.dataobject.TeamDO;
import stpmproject.error.BusinessException;
import stpmproject.error.EmBusinessError;
import stpmproject.service.TeamService;
import stpmproject.service.model.TeamModel;
import stpmproject.validator.ValidatorImpl;
import stpmproject.validator.ValidatorResult;

import java.util.List;
import java.util.stream.Collectors;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/23 17:00
 * @software: IntelliJ IDEA
 * @file: TeamServiceImpl
 * @description:
 */
@Service
public class TeamServiceImpl implements TeamService {
    @Autowired
    private ValidatorImpl validator;

    @Autowired
    private TeamDOMapper teamDOMapper;

    @Override
    public TeamModel getTeamById(Integer teamId) {
        return null;
    }

    @Override
    public TeamModel getTeamByProjectCode(String projectCode) {
        TeamDO teamDO=teamDOMapper.selectByProjectCode(projectCode);
        if(teamDO==null){
            return null;
        }
        return convertModelFromDao(teamDO);
    }

    @Override
    public List<TeamModel> listTeam() {
        List<TeamDO> teamDOList=teamDOMapper.listTeam();
        List<TeamModel> teamModelList=teamDOList.stream().map(teamDO -> {
            TeamModel teamModel=this.convertModelFromDao(teamDO);
            return teamModel;
        }).collect(Collectors.toList());
        return teamModelList;
    }

    @Override
    public void createTeam(TeamModel teamModel) throws BusinessException {
        if(teamModel==null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        ValidatorResult result=validator.validate(teamModel);
        if(result.getHasErr()==true){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,result.getErrMsg());
        }
        TeamDO teamDO=convertDaoFromModel(teamModel);
        try{
            teamDOMapper.insertSelective(teamDO);
        }catch (DuplicateKeyException e){
            e.printStackTrace();
            throw new BusinessException(EmBusinessError.TEAM_EXIST,"团队名已经被注册");
        }
        return;
    }

    @Override
    public boolean updateTeam(TeamModel teamModel) {
        TeamDO teamDO=convertDaoFromModel(teamModel);
        int affectRow=teamDOMapper.updateByPrimaryKeySelective(teamDO);
        return affectRow>0?true:false;
    }

    @Override
    public TeamModel getTeamByProjectAndName(String teamProject, String teamName) throws BusinessException {
        TeamDO teamDO=teamDOMapper.selectByProjectAndName(teamProject,teamName);
        return convertModelFromDao(teamDO);
    }

    private TeamModel convertModelFromDao(TeamDO teamDO){
        if(teamDO==null){
            return null;
        }
        TeamModel teamModel=new TeamModel();
        BeanUtils.copyProperties(teamDO,teamModel);
        teamModel.setTeamCreateDate(new DateTime(teamDO.getTeamCreateDate()));
        return teamModel;
    }
    private TeamDO convertDaoFromModel(TeamModel teamModel){
        if(teamModel==null){
            return null;
        }
        TeamDO teamDO=new TeamDO();
        BeanUtils.copyProperties(teamModel,teamDO);
        return teamDO;
    }
}
