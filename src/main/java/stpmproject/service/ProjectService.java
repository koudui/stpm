package stpmproject.service;

import stpmproject.error.BusinessException;
import stpmproject.service.model.ProjectModel;

import java.util.List;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/21 19:43
 * @software: IntelliJ IDEA
 * @file: ProjectService
 * @description:
 */
public interface ProjectService {
    ProjectModel getProjectById(Integer id);

    ProjectModel getProjectByCode(String projectCode);

    List<ProjectModel> listProject();

    void createProject(ProjectModel projectModel) throws BusinessException;

    boolean deleteProject(Integer projectId);

    boolean updateProject(ProjectModel projectModel) throws BusinessException;
}
