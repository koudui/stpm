package stpmproject.service.model;

import org.joda.time.DateTime;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 13:30
 * @software: IntelliJ IDEA
 * @file: BugModel
 * @description:
 */
public class BugModel {
    private Integer id;
    private String bugName;
    private String bugProjectCode;
    private String bugPriority;
    private DateTime bugStartDate;
    private DateTime bugEndDate;
    private DateTime bugCreateDate;
    private String bugCreater;
    private String bugAssginedTo;
    private String bugKeyword;
    private String bugStep;
    private String bugStatus;
    //
    private Integer numOfProject;

    public Integer getNumOfProject() {
        return numOfProject;
    }

    public void setNumOfProject(Integer numOfProject) {
        this.numOfProject = numOfProject;
    }

    public DateTime getBugCreateDate() {
        return bugCreateDate;
    }

    public void setBugCreateDate(DateTime bugCreateDate) {
        this.bugCreateDate = bugCreateDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBugName() {
        return bugName;
    }

    public void setBugName(String bugName) {
        this.bugName = bugName;
    }

    public String getBugProjectCode() {
        return bugProjectCode;
    }

    public void setBugProjectCode(String bugProjectCode) {
        this.bugProjectCode = bugProjectCode;
    }

    public String getBugPriority() {
        return bugPriority;
    }

    public void setBugPriority(String bugPriority) {
        this.bugPriority = bugPriority;
    }

    public DateTime getBugStartDate() {
        return bugStartDate;
    }

    public void setBugStartDate(DateTime bugStartDate) {
        this.bugStartDate = bugStartDate;
    }

    public DateTime getBugEndDate() {
        return bugEndDate;
    }

    public void setBugEndDate(DateTime bugEndDate) {
        this.bugEndDate = bugEndDate;
    }

    public String getBugCreater() {
        return bugCreater;
    }

    public void setBugCreater(String bugCreater) {
        this.bugCreater = bugCreater;
    }

    public String getBugAssginedTo() {
        return bugAssginedTo;
    }

    public void setBugAssginedTo(String bugAssginedTo) {
        this.bugAssginedTo = bugAssginedTo;
    }

    public String getBugKeyword() {
        return bugKeyword;
    }

    public void setBugKeyword(String bugKeyword) {
        this.bugKeyword = bugKeyword;
    }

    public String getBugStep() {
        return bugStep;
    }

    public void setBugStep(String bugStep) {
        this.bugStep = bugStep;
    }

    public String getBugStatus() {
        return bugStatus;
    }

    public void setBugStatus(String bugStatus) {
        this.bugStatus = bugStatus;
    }
}
