package stpmproject.service.model;

import org.joda.time.DateTime;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 23:53
 * @software: IntelliJ IDEA
 * @file: TodoModel
 * @description:
 */
public class TodoModel {
    private Integer id;
    private String todoName;
    private String todoPriority;
    private DateTime todoCreateDate;
    private DateTime todoStartDate;
    private DateTime todoEndDate;
    private String todoDesc;
    private String todoStatus;
    private String todoCreater;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTodoName() {
        return todoName;
    }

    public void setTodoName(String todoName) {
        this.todoName = todoName;
    }

    public String getTodoPriority() {
        return todoPriority;
    }

    public void setTodoPriority(String todoPriority) {
        this.todoPriority = todoPriority;
    }

    public DateTime getTodoCreateDate() {
        return todoCreateDate;
    }

    public void setTodoCreateDate(DateTime todoCreateDate) {
        this.todoCreateDate = todoCreateDate;
    }

    public DateTime getTodoStartDate() {
        return todoStartDate;
    }

    public void setTodoStartDate(DateTime todoStartDate) {
        this.todoStartDate = todoStartDate;
    }

    public DateTime getTodoEndDate() {
        return todoEndDate;
    }

    public void setTodoEndDate(DateTime todoEndDate) {
        this.todoEndDate = todoEndDate;
    }

    public String getTodoDesc() {
        return todoDesc;
    }

    public void setTodoDesc(String todoDesc) {
        this.todoDesc = todoDesc;
    }

    public String getTodoStatus() {
        return todoStatus;
    }

    public void setTodoStatus(String todoStatus) {
        this.todoStatus = todoStatus;
    }

    public String getTodoCreater() {
        return todoCreater;
    }

    public void setTodoCreater(String todoCreater) {
        this.todoCreater = todoCreater;
    }
}
