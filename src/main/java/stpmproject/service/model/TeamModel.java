package stpmproject.service.model;

import org.joda.time.DateTime;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/23 16:49
 * @software: IntelliJ IDEA
 * @file: TeamModel
 * @description:
 */
public class TeamModel {
    private Integer id;
    private String teamName;
    private String teamProject;
    private String teamDesc;
    private String teamProgramer;
    private String teamDeveloper;
    private String teamTester;
    private DateTime teamCreateDate;
    private String teamStatus;

    public String getTeamStatus() {
        return teamStatus;
    }

    public void setTeamStatus(String teamStatus) {
        this.teamStatus = teamStatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamProject() {
        return teamProject;
    }

    public void setTeamProject(String teamProject) {
        this.teamProject = teamProject;
    }

    public String getTeamDesc() {
        return teamDesc;
    }

    public void setTeamDesc(String teamDesc) {
        this.teamDesc = teamDesc;
    }

    public String getTeamProgramer() {
        return teamProgramer;
    }

    public void setTeamProgramer(String teamProgramer) {
        this.teamProgramer = teamProgramer;
    }

    public String getTeamDeveloper() {
        return teamDeveloper;
    }

    public void setTeamDeveloper(String teamDeveloper) {
        this.teamDeveloper = teamDeveloper;
    }

    public String getTeamTester() {
        return teamTester;
    }

    public void setTeamTester(String teamTester) {
        this.teamTester = teamTester;
    }

    public DateTime getTeamCreateDate() {
        return teamCreateDate;
    }

    public void setTeamCreateDate(DateTime teamCreateDate) {
        this.teamCreateDate = teamCreateDate;
    }
}
