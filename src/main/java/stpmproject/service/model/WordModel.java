package stpmproject.service.model;

import org.joda.time.DateTime;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 20:29
 * @software: IntelliJ IDEA
 * @file: WordModel
 * @description:
 */
public class WordModel {
    private Integer id;
    private String wordTitle;
    private String wordType;
    private String wordKeyword;
    private String wordCreater;
    private String wordContent;
    private DateTime wordCreateDate;
    private String wordLib;
    private DateTime wordEditDate;
    private String wordEditer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWordTitle() {
        return wordTitle;
    }

    public void setWordTitle(String wordTitle) {
        this.wordTitle = wordTitle;
    }

    public String getWordType() {
        return wordType;
    }

    public void setWordType(String wordType) {
        this.wordType = wordType;
    }

    public String getWordKeyword() {
        return wordKeyword;
    }

    public void setWordKeyword(String wordKeyword) {
        this.wordKeyword = wordKeyword;
    }

    public String getWordCreater() {
        return wordCreater;
    }

    public void setWordCreater(String wordCreater) {
        this.wordCreater = wordCreater;
    }

    public String getWordContent() {
        return wordContent;
    }

    public void setWordContent(String wordContent) {
        this.wordContent = wordContent;
    }

    public DateTime getWordCreateDate() {
        return wordCreateDate;
    }

    public void setWordCreateDate(DateTime wordCreateDate) {
        this.wordCreateDate = wordCreateDate;
    }

    public String getWordLib() {
        return wordLib;
    }

    public void setWordLib(String wordLib) {
        this.wordLib = wordLib;
    }

    public DateTime getWordEditDate() {
        return wordEditDate;
    }

    public void setWordEditDate(DateTime wordEditDate) {
        this.wordEditDate = wordEditDate;
    }

    public String getWordEditer() {
        return wordEditer;
    }

    public void setWordEditer(String wordEditer) {
        this.wordEditer = wordEditer;
    }
}
