package stpmproject.service.model;

import org.joda.time.DateTime;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/25 22:52
 * @software: IntelliJ IDEA
 * @file: TaskModel
 * @description:
 */
public class TaskModel {
    private Integer id;
    private String taskProjectCode;
    private String taskPriority;
    private String taskName;
    private String taskKeyword;
    private DateTime taskEndDate;
    private DateTime taskStartDate;
    private String taskDesc;
    private String taskStatus;
    private String taskAssginedTo;
    private String taskCreater;

    //
    private Integer numOfProject;

    public Integer getNumOfProject() {
        return numOfProject;
    }

    public void setNumOfProject(Integer numOfProject) {
        this.numOfProject = numOfProject;
    }

    public DateTime getTaskStartDate() {
        return taskStartDate;
    }

    public void setTaskStartDate(DateTime taskStartDate) {
        this.taskStartDate = taskStartDate;
    }

    public String getTaskProjectCode() {
        return taskProjectCode;
    }

    public void setTaskProjectCode(String taskProjectCode) {
        this.taskProjectCode = taskProjectCode;
    }

    public String getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(String taskPriority) {
        this.taskPriority = taskPriority;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskKeyword() {
        return taskKeyword;
    }

    public void setTaskKeyword(String taskKeyword) {
        this.taskKeyword = taskKeyword;
    }

    public DateTime getTaskEndDate() {
        return taskEndDate;
    }

    public void setTaskEndDate(DateTime taskEndDate) {
        this.taskEndDate = taskEndDate;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }


    public String getTaskCreater() {
        return taskCreater;
    }

    public void setTaskCreater(String taskCreater) {
        this.taskCreater = taskCreater;
    }

    public String getTaskAssginedTo() {
        return taskAssginedTo;
    }

    public void setTaskAssginedTo(String taskAssginedTo) {
        this.taskAssginedTo = taskAssginedTo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
