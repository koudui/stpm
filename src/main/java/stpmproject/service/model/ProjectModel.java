package stpmproject.service.model;

import org.joda.time.DateTime;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/21 17:21
 * @software: IntelliJ IDEA
 * @file: ProjectModel
 * @description:
 */
public class ProjectModel {
    private Integer id;
    private String projectName;
    private String projectPriority;
    private String projectKeyword;
    private DateTime projectCreateDate;
    private DateTime projectStartDate;
    private DateTime projectEndDate;
    private Integer hours;
    private String projectDesc;
    private String projectCreater;
    private String projectStatus;
    private String projectCode;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectPriority() {
        return projectPriority;
    }

    public void setProjectPriority(String projectPriority) {
        this.projectPriority = projectPriority;
    }

    public String getProjectKeyword() {
        return projectKeyword;
    }

    public void setProjectKeyword(String projectKeyword) {
        this.projectKeyword = projectKeyword;
    }

    public DateTime getProjectCreateDate() {
        return projectCreateDate;
    }

    public void setProjectCreateDate(DateTime projectCreateDate) {
        this.projectCreateDate = projectCreateDate;
    }

    public DateTime getProjectStartDate() {
        return projectStartDate;
    }

    public void setProjectStartDate(DateTime projectStartDate) {
        this.projectStartDate = projectStartDate;
    }

    public DateTime getProjectEndDate() {
        return projectEndDate;
    }

    public void setProjectEndDate(DateTime projectEndDate) {
        this.projectEndDate = projectEndDate;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public String getProjectDesc() {
        return projectDesc;
    }

    public void setProjectDesc(String projectDesc) {
        this.projectDesc = projectDesc;
    }

    public String getProjectCreater() {
        return projectCreater;
    }

    public void setProjectCreater(String projectCreater) {
        this.projectCreater = projectCreater;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }
}
