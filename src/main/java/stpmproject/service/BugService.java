package stpmproject.service;

import stpmproject.error.BusinessException;
import stpmproject.service.model.BugModel;

import java.util.List;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 13:39
 * @software: IntelliJ IDEA
 * @file: BugService
 * @description:
 */
public interface BugService {
    BugModel getBugById(Integer bugId);

    List<BugModel> listBug();

    List<BugModel> listMyBug(String assginedTo);

    List<BugModel> groupBugByUserName(String userName);

    void createBug(BugModel bugModel) throws BusinessException;

    void updateBug(BugModel bugModel) throws BusinessException;

    void deleteBugById(Integer bugId) throws BusinessException;

    BugModel getBugByNameAndProjectCode(String bugName,String bugProjectCode,String assginedTo);
}
