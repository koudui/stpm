package stpmproject.service;

import stpmproject.error.BusinessException;
import stpmproject.service.model.ProjectModel;
import stpmproject.service.model.TaskModel;

import java.util.List;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/25 23:09
 * @software: IntelliJ IDEA
 * @file: TaskService
 * @description:
 */
public interface TaskService {
    TaskModel getTaskById(Integer taskId);

    List<ProjectModel> getTaskByAssgined(String userName);

    List<TaskModel> getTaskByAssginedTo(String userName);

    TaskModel getByProjectAndNameAndTeam(String project,String name,String team) throws BusinessException;

    List<TaskModel> listTask();

    List<TaskModel> groupByUserName(String userName);

    void createTask(TaskModel taskModel) throws BusinessException;

    void updateTask(TaskModel taskModel) throws BusinessException;

    void deleteTaskById(Integer taskId) throws BusinessException;
}
