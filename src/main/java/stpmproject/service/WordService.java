package stpmproject.service;

import stpmproject.error.BusinessException;
import stpmproject.service.model.WordModel;

import java.util.List;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 20:35
 * @software: IntelliJ IDEA
 * @file: WordService
 * @description:
 */
public interface WordService {
    WordModel getWordById(Integer wordId) throws BusinessException;

    WordModel getWordByTitle(String wordTitle);

    List<WordModel> listWord();

    void createWord(WordModel wordModel) throws BusinessException;

    void updateWord(WordModel wordModel) throws BusinessException;

    void deleteWord(Integer wordId) throws BusinessException;
}
