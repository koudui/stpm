package stpmproject.dataobject;

import java.util.Date;

public class TeamDO {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_info.id
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_info.team_name
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    private String teamName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_info.team_project
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    private String teamProject;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_info.team_desc
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    private String teamDesc;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_info.team_programer
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    private String teamProgramer;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_info.team_developer
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    private String teamDeveloper;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_info.team_tester
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    private String teamTester;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_info.team_status
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    private String teamStatus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column team_info.team_create_date
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    private Date teamCreateDate;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_info.id
     *
     * @return the value of team_info.id
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_info.id
     *
     * @param id the value for team_info.id
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_info.team_name
     *
     * @return the value of team_info.team_name
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_info.team_name
     *
     * @param teamName the value for team_info.team_name
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public void setTeamName(String teamName) {
        this.teamName = teamName == null ? null : teamName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_info.team_project
     *
     * @return the value of team_info.team_project
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public String getTeamProject() {
        return teamProject;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_info.team_project
     *
     * @param teamProject the value for team_info.team_project
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public void setTeamProject(String teamProject) {
        this.teamProject = teamProject == null ? null : teamProject.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_info.team_desc
     *
     * @return the value of team_info.team_desc
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public String getTeamDesc() {
        return teamDesc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_info.team_desc
     *
     * @param teamDesc the value for team_info.team_desc
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public void setTeamDesc(String teamDesc) {
        this.teamDesc = teamDesc == null ? null : teamDesc.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_info.team_programer
     *
     * @return the value of team_info.team_programer
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public String getTeamProgramer() {
        return teamProgramer;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_info.team_programer
     *
     * @param teamProgramer the value for team_info.team_programer
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public void setTeamProgramer(String teamProgramer) {
        this.teamProgramer = teamProgramer == null ? null : teamProgramer.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_info.team_developer
     *
     * @return the value of team_info.team_developer
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public String getTeamDeveloper() {
        return teamDeveloper;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_info.team_developer
     *
     * @param teamDeveloper the value for team_info.team_developer
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public void setTeamDeveloper(String teamDeveloper) {
        this.teamDeveloper = teamDeveloper == null ? null : teamDeveloper.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_info.team_tester
     *
     * @return the value of team_info.team_tester
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public String getTeamTester() {
        return teamTester;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_info.team_tester
     *
     * @param teamTester the value for team_info.team_tester
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public void setTeamTester(String teamTester) {
        this.teamTester = teamTester == null ? null : teamTester.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_info.team_status
     *
     * @return the value of team_info.team_status
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public String getTeamStatus() {
        return teamStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_info.team_status
     *
     * @param teamStatus the value for team_info.team_status
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public void setTeamStatus(String teamStatus) {
        this.teamStatus = teamStatus == null ? null : teamStatus.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column team_info.team_create_date
     *
     * @return the value of team_info.team_create_date
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public Date getTeamCreateDate() {
        return teamCreateDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column team_info.team_create_date
     *
     * @param teamCreateDate the value for team_info.team_create_date
     *
     * @mbggenerated Thu May 23 16:54:30 CST 2019
     */
    public void setTeamCreateDate(Date teamCreateDate) {
        this.teamCreateDate = teamCreateDate;
    }
}