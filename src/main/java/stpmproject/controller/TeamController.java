package stpmproject.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import stpmproject.controller.viewobject.TeamVO;
import stpmproject.error.BusinessException;
import stpmproject.error.EmBusinessError;
import stpmproject.response.CommonReturnType;
import stpmproject.service.TeamService;
import stpmproject.service.model.TeamModel;
import stpmproject.service.model.UserModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/23 17:09
 * @software: IntelliJ IDEA
 * @file: TeamController
 * @description:
 */
@Controller("team")
@RequestMapping("/team")
@CrossOrigin(allowedHeaders = {"*"},allowCredentials = "true") //springboot前端请求的跨域问题
public class TeamController extends BaseController {
    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private TeamService teamService;

    @RequestMapping("/get")
    @ResponseBody
    public CommonReturnType getTeam(@RequestParam(name = "id") Integer teamId) throws BusinessException {
        TeamModel teamModel=teamService.getTeamById(teamId);
        if(teamModel==null){
            throw new BusinessException(EmBusinessError.TEAM_NOT_EXIST);
        }
        TeamVO teamVO=convertVOFromModel(teamModel);
        return CommonReturnType.create(teamVO);
    }

    private TeamVO convertVOFromModel(TeamModel teamModel){
        if(teamModel==null){
            return null;
        }
        TeamVO teamVO=new TeamVO();
        BeanUtils.copyProperties(teamModel,teamVO);
        return teamVO;
    }

    //获取团队列表,开启分页功能
    @RequestMapping(value = "/list",method = {RequestMethod.GET})
    @ResponseBody
    public CommonReturnType listTeam(@RequestParam(name = "page") Integer page,
                                     @RequestParam(name = "limit") Integer limit){
        //开启分页
        Page tmpPage=PageHelper.startPage(page,limit);
        List<TeamModel> teamModelList=teamService.listTeam();
        List<TeamVO> teamVOList=teamModelList.stream().map(teamModel -> {
            TeamVO teamVO=this.convertVOFromModel(teamModel);
            return teamVO;
        }).collect(Collectors.toList());
        return CommonReturnType.create(teamVOList,tmpPage.getTotal());
    }

    @RequestMapping(value = "/name",method = {RequestMethod.GET})
    @ResponseBody
    public CommonReturnType teamNameList(){
        List<TeamModel> teamModelList=teamService.listTeam();
        List<TeamVO> teamVOList=teamModelList.stream().map(teamModel -> {
            TeamVO teamVO=new TeamVO();
            teamVO.setName(teamModel.getTeamProject()+"-"+teamModel.getTeamName()+"-"+teamModel.getTeamStatus());
            teamVO.setValue(teamModel.getTeamName());
            return teamVO;
        }).collect(Collectors.toList());
        return CommonReturnType.create(teamVOList);
    }

    //创建团队接口
    @RequestMapping(value = "/create",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMAT})
    @ResponseBody
    public CommonReturnType createTeam(@RequestParam(name = "teamProject") String teamProject,
                                       @RequestParam(name = "teamName") String teamName,
                                       @RequestParam(name = "teamDesc") String teamDesc,
                                       @RequestParam(name = "developerSelect") String teamDeveloper,
                                       @RequestParam(name = "testerSelect") String teamTester,
                                       @RequestParam(name = "type") String teamStatus,
                                       HttpSession session) throws BusinessException {
        UserModel userModel=(UserModel) session.getAttribute("LOGIN_USER");
        if(userModel==null){
            throw new BusinessException(EmBusinessError.USER_NOT_LOGIN);
        }
        TeamModel model=teamService.getTeamByProjectAndName(teamProject,teamName);
        if(model!=null){
            throw new BusinessException(EmBusinessError.TEAM_EXIST);
        }
        if(!userModel.getRole().equals("项目经理")){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,"你没有权限创建");
        }

        //一个项目中团队名不能重复
        TeamModel teamModel=new TeamModel();
        teamModel.setTeamProject(teamProject);
        teamModel.setTeamName(teamName);
        teamModel.setTeamDesc(teamDesc);
        teamModel.setTeamProgramer(userModel.getUsername());
        teamModel.setTeamDeveloper(teamDeveloper);
        teamModel.setTeamTester(teamTester);
        teamModel.setTeamStatus(teamStatus);
        teamService.createTeam(teamModel);
        return CommonReturnType.create(null);
    }

    //创建团队接口
    @RequestMapping(value = "/update",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMAT})
    @ResponseBody
    public CommonReturnType updateTeam(@RequestParam(name = "teamProject_edit") String teamProject,
                                          @RequestParam(name = "teamName_edit") String teamName,
                                          @RequestParam(name = "teamDesc_edit") String teamDesc,
                                          @RequestParam(name = "programerSelect_edit") String teamProgramer,
                                          @RequestParam(name = "developerSelect_edit") String teamDeveloper,
                                          @RequestParam(name = "testerSelect_edit") String teamTester,
                                          @RequestParam(name = "type_edit") String teamStatus,
                                       HttpSession session) throws BusinessException {
        UserModel userModel=(UserModel) session.getAttribute("LOGIN_USER");
        if(userModel==null){
            throw new BusinessException(EmBusinessError.USER_NOT_LOGIN);
        }
        //一个项目中团队名不能重复
        TeamModel teamModel=teamService.getTeamByProjectAndName(teamProject,teamName);
        if(teamModel==null){
            throw  new BusinessException(EmBusinessError.TEAM_NOT_EXIST);
        }
        if(!userModel.getRole().equals("项目经理")){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,"你没有权限创建");
        }
        System.out.println("teamProject_edit = " + teamProject);
        teamModel.setTeamDesc(teamDesc);
        //如果前端没有勾选成员，则不修改原有的团队表
        if(teamProgramer!="") {
            teamModel.setTeamProgramer(teamProgramer);
        }
        if(teamDeveloper!="") {
            teamModel.setTeamDeveloper(teamDeveloper);
        }
        if(teamTester!="") {
            teamModel.setTeamTester(teamTester);
        }
        teamModel.setTeamStatus(teamStatus);
        teamService.updateTeam(teamModel);
        return CommonReturnType.create(null);
    }
}
