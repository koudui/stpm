package stpmproject.controller.viewobject;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/21 20:23
 * @software: IntelliJ IDEA
 * @file: ProjectVO
 * @description:
 */
public class ProjectVO {
    private Integer id;
    private String projectName;
    private String projectPriority;
//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date projectStartDate;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date projectEndDate;
    private Integer hours;
    private String projectDesc;
    private String projectCreater;
    private String projectStatus;
    private String totalHours;
    private String usedHours;
    private String leftHours;
    private String projectCode;
    private String projectProcess;
    //项目经理角色
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectPriority() {
        return projectPriority;
    }

    public void setProjectPriority(String projectPriority) {
        this.projectPriority = projectPriority;
    }

    public Date getProjectStartDate() {
        return projectStartDate;
    }

    public void setProjectStartDate(Date projectStartDate) {
        this.projectStartDate = projectStartDate;
    }

    public Date getProjectEndDate() {
        return projectEndDate;
    }

    public void setProjectEndDate(Date projectEndDate) {
        this.projectEndDate = projectEndDate;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public String getProjectDesc() {
        return projectDesc;
    }

    public void setProjectDesc(String projectDesc) {
        this.projectDesc = projectDesc;
    }

    public String getProjectCreater() {
        return projectCreater;
    }

    public void setProjectCreater(String projectCreater) {
        this.projectCreater = projectCreater;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getUsedHours() {
        return usedHours;
    }

    public void setUsedHours(String usedHours) {
        this.usedHours = usedHours;
    }

    public String getLeftHours() {
        return leftHours;
    }

    public void setLeftHours(String leftHours) {
        this.leftHours = leftHours;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectProcess() {
        return projectProcess;
    }

    public void setProjectProcess(String projectProcess) {
        this.projectProcess = projectProcess;
    }
}
