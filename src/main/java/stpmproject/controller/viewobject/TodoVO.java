package stpmproject.controller.viewobject;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 23:48
 * @software: IntelliJ IDEA
 * @file: TodoVO
 * @description:
 */
public class TodoVO {
    private Integer id;
    private String todoName;
    private String todoPriority;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date todoCreateDate;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date todoStartDate;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date todoEndDate;
    private String todoDesc;
    private String todoStatus;
    private String todoCreater;
    //
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTodoName() {
        return todoName;
    }

    public void setTodoName(String todoName) {
        this.todoName = todoName;
    }

    public String getTodoPriority() {
        return todoPriority;
    }

    public void setTodoPriority(String todoPriority) {
        this.todoPriority = todoPriority;
    }

    public Date getTodoCreateDate() {
        return todoCreateDate;
    }

    public void setTodoCreateDate(Date todoCreateDate) {
        this.todoCreateDate = todoCreateDate;
    }

    public Date getTodoStartDate() {
        return todoStartDate;
    }

    public void setTodoStartDate(Date todoStartDate) {
        this.todoStartDate = todoStartDate;
    }

    public Date getTodoEndDate() {
        return todoEndDate;
    }

    public void setTodoEndDate(Date todoEndDate) {
        this.todoEndDate = todoEndDate;
    }

    public String getTodoDesc() {
        return todoDesc;
    }

    public void setTodoDesc(String todoDesc) {
        this.todoDesc = todoDesc;
    }

    public String getTodoStatus() {
        return todoStatus;
    }

    public void setTodoStatus(String todoStatus) {
        this.todoStatus = todoStatus;
    }

    public String getTodoCreater() {
        return todoCreater;
    }

    public void setTodoCreater(String todoCreater) {
        this.todoCreater = todoCreater;
    }
}
