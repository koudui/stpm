package stpmproject.controller.viewobject;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 20:34
 * @software: IntelliJ IDEA
 * @file: WordVO
 * @description:
 */
public class WordVO {
    private Integer id;
    private String wordTitle;
    private String wordType;
    private String wordCreater;
    private String wordContent;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date wordCreateDate;
    private String wordLib;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date wordEditDate;
    private String wordEditer;
    private String wordKeyword;

    public String getWordKeyword() {
        return wordKeyword;
    }

    public void setWordKeyword(String wordKeyword) {
        this.wordKeyword = wordKeyword;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWordTitle() {
        return wordTitle;
    }

    public void setWordTitle(String wordTitle) {
        this.wordTitle = wordTitle;
    }

    public String getWordType() {
        return wordType;
    }

    public void setWordType(String wordType) {
        this.wordType = wordType;
    }

    public String getWordCreater() {
        return wordCreater;
    }

    public void setWordCreater(String wordCreater) {
        this.wordCreater = wordCreater;
    }

    public String getWordContent() {
        return wordContent;
    }

    public void setWordContent(String wordContent) {
        this.wordContent = wordContent;
    }

    public Date getWordCreateDate() {
        return wordCreateDate;
    }

    public void setWordCreateDate(Date wordCreateDate) {
        this.wordCreateDate = wordCreateDate;
    }

    public String getWordLib() {
        return wordLib;
    }

    public void setWordLib(String wordLib) {
        this.wordLib = wordLib;
    }

    public Date getWordEditDate() {
        return wordEditDate;
    }

    public void setWordEditDate(Date wordEditDate) {
        this.wordEditDate = wordEditDate;
    }

    public String getWordEditer() {
        return wordEditer;
    }

    public void setWordEditer(String wordEditer) {
        this.wordEditer = wordEditer;
    }
}
