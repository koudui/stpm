package stpmproject.controller.viewobject;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 13:37
 * @software: IntelliJ IDEA
 * @file: BugVO
 * @description:
 */
public class BugVO {
    private Integer id;
    private String bugName;
    private String bugProjectCode;
    private String bugPriority;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date bugStartDate;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date bugEndDate;
    private String bugCreater;
    private String bugAssginedTo;
    private String bugKeyword;
    private String bugStep;
    private String bugStatus;
    private String role;

    private String userName;
    private String realName;
    private Integer numOfProject;

    public Integer getNumOfProject() {
        return numOfProject;
    }

    public void setNumOfProject(Integer numOfProject) {
        this.numOfProject = numOfProject;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBugName() {
        return bugName;
    }

    public void setBugName(String bugName) {
        this.bugName = bugName;
    }

    public String getBugProjectCode() {
        return bugProjectCode;
    }

    public void setBugProjectCode(String bugProjectCode) {
        this.bugProjectCode = bugProjectCode;
    }

    public String getBugPriority() {
        return bugPriority;
    }

    public void setBugPriority(String bugPriority) {
        this.bugPriority = bugPriority;
    }

    public Date getBugStartDate() {
        return bugStartDate;
    }

    public void setBugStartDate(Date bugStartDate) {
        this.bugStartDate = bugStartDate;
    }

    public Date getBugEndDate() {
        return bugEndDate;
    }

    public void setBugEndDate(Date bugEndDate) {
        this.bugEndDate = bugEndDate;
    }

    public String getBugCreater() {
        return bugCreater;
    }

    public void setBugCreater(String bugCreater) {
        this.bugCreater = bugCreater;
    }

    public String getBugAssginedTo() {
        return bugAssginedTo;
    }

    public void setBugAssginedTo(String bugAssginedTo) {
        this.bugAssginedTo = bugAssginedTo;
    }

    public String getBugKeyword() {
        return bugKeyword;
    }

    public void setBugKeyword(String bugKeyword) {
        this.bugKeyword = bugKeyword;
    }

    public String getBugStep() {
        return bugStep;
    }

    public void setBugStep(String bugStep) {
        this.bugStep = bugStep;
    }

    public String getBugStatus() {
        return bugStatus;
    }

    public void setBugStatus(String bugStatus) {
        this.bugStatus = bugStatus;
    }
}
