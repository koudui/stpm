package stpmproject.controller.viewobject;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/6/1 15:27
 * @software: IntelliJ IDEA
 * @file: TotalVO
 * @description:
 */
public class TotalVO {
    private Integer totalTask;
    private Integer totalBug;
    private Integer totalProject;
    private Integer totalTodo;

    public Integer getTotalTask() {
        return totalTask;
    }

    public void setTotalTask(Integer totalTask) {
        this.totalTask = totalTask;
    }

    public Integer getTotalBug() {
        return totalBug;
    }

    public void setTotalBug(Integer totalBug) {
        this.totalBug = totalBug;
    }

    public Integer getTotalProject() {
        return totalProject;
    }

    public void setTotalProject(Integer totalProject) {
        this.totalProject = totalProject;
    }

    public Integer getTotalTodo() {
        return totalTodo;
    }

    public void setTotalTodo(Integer totalTodo) {
        this.totalTodo = totalTodo;
    }
}
