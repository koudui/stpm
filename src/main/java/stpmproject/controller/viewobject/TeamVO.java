package stpmproject.controller.viewobject;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/23 16:52
 * @software: IntelliJ IDEA
 * @file: TeamVO
 * @description:
 */
public class TeamVO {
    private Integer id;
    private String teamName;
    private String teamProject;
    private String teamDesc;
    private String teamProgramer;
    private String teamDeveloper;
    private String teamTester;
    private String teamStatus;
    //多选下拉框赋值
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTeamStatus() {
        return teamStatus;
    }

    public void setTeamStatus(String teamStatus) {
        this.teamStatus = teamStatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamProject() {
        return teamProject;
    }

    public void setTeamProject(String teamProject) {
        this.teamProject = teamProject;
    }

    public String getTeamDesc() {
        return teamDesc;
    }

    public void setTeamDesc(String teamDesc) {
        this.teamDesc = teamDesc;
    }

    public String getTeamProgramer() {
        return teamProgramer;
    }

    public void setTeamProgramer(String teamProgramer) {
        this.teamProgramer = teamProgramer;
    }

    public String getTeamDeveloper() {
        return teamDeveloper;
    }

    public void setTeamDeveloper(String teamDeveloper) {
        this.teamDeveloper = teamDeveloper;
    }

    public String getTeamTester() {
        return teamTester;
    }

    public void setTeamTester(String teamTester) {
        this.teamTester = teamTester;
    }
}
