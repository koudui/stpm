package stpmproject.controller.viewobject;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/25 22:56
 * @software: IntelliJ IDEA
 * @file: TaskVO
 * @description:
 */
public class TaskVO {
    private Integer id;
    private String taskProjectCode;
    private String taskPriority;
    private String taskName;
    private String taskKeyword;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date taskEndDate;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date taskStartDate;
    private String taskDesc;
    private String taskStatus;
    private String taskAssginedTo;
    private String taskCreater;
    //保存登录用户的角色
    private String role;
    //每个用户在一个项目中的任务数
    private Integer totalTask;

    private String assginedTo;

    private String realName;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getAssginedTo() {
        return assginedTo;
    }

    public void setAssginedTo(String assginedTo) {
        this.assginedTo = assginedTo;
    }

    public Integer getTotalTask() {
        return totalTask;
    }

    public void setTotalTask(Integer totalTask) {
        this.totalTask = totalTask;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getTaskCreater() {
        return taskCreater;
    }

    public void setTaskCreater(String taskCreater) {
        this.taskCreater = taskCreater;
    }

    public Date getTaskStartDate() {
        return taskStartDate;
    }

    public void setTaskStartDate(Date taskStartDate) {
        this.taskStartDate = taskStartDate;
    }

    public String getTaskProjectCode() {
        return taskProjectCode;
    }

    public void setTaskProjectCode(String taskProjectCode) {
        this.taskProjectCode = taskProjectCode;
    }

    public String getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(String taskPriority) {
        this.taskPriority = taskPriority;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskKeyword() {
        return taskKeyword;
    }

    public void setTaskKeyword(String taskKeyword) {
        this.taskKeyword = taskKeyword;
    }

    public Date getTaskEndDate() {
        return taskEndDate;
    }

    public void setTaskEndDate(Date taskEndDate) {
        this.taskEndDate = taskEndDate;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskAssginedTo() {
        return taskAssginedTo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTaskAssginedTo(String taskAssginedTo) {
        this.taskAssginedTo = taskAssginedTo;
    }
}
