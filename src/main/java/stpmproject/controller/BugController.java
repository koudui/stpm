package stpmproject.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import stpmproject.controller.viewobject.BugVO;
import stpmproject.error.BusinessException;
import stpmproject.error.EmBusinessError;
import stpmproject.response.CommonReturnType;
import stpmproject.service.BugService;
import stpmproject.service.UserService;
import stpmproject.service.model.BugModel;
import stpmproject.service.model.UserModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 13:59
 * @software: IntelliJ IDEA
 * @file: BugController
 * @description:
 */
@Controller("bug")
@RequestMapping("/bug")
@CrossOrigin(allowedHeaders = {"*"},allowCredentials = "true") //springboot前端请求的跨域问题
public class BugController extends BaseController{
    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private BugService bugService;

    @Autowired
    private UserService userService;

    @RequestMapping("/get")
    @ResponseBody
    public CommonReturnType getBug(@RequestParam(name = "id") Integer bugId) throws BusinessException {
        BugModel bugModel=bugService.getBugById(bugId);
        if(bugModel==null){
            throw new BusinessException(EmBusinessError.BUG_NOT_EXIST);
        }
        BugVO bugVO=convertVOFromModel(bugModel);
        return CommonReturnType.create(bugVO);
    }

    //获取问题单列表,开启分页功能
    @RequestMapping(value = "/list",method = {RequestMethod.GET})
    @ResponseBody
    public CommonReturnType listBug(@RequestParam(name = "page") Integer page,
                                    @RequestParam(name = "limit") Integer limit){
        Page tmpPage=PageHelper.startPage(page,limit);
        List<BugModel> bugModelList=bugService.listBug();
        List<BugVO> bugVOList=bugModelList.stream().map(bugModel -> {
            BugVO bugVO=this.convertVOFromModel(bugModel);
            return bugVO;
        }).collect(Collectors.toList());
        return CommonReturnType.create(bugVOList,tmpPage.getTotal());
    }
    //获取问题单列表,开启分页功能
    @RequestMapping(value = "/mybug",method = {RequestMethod.GET})
    @ResponseBody
    public CommonReturnType listMyBug(@RequestParam(name = "page") Integer page,
                                      @RequestParam(name = "limit") Integer limit,
                                      HttpSession session) throws BusinessException {
        UserModel userModel= (UserModel) session.getAttribute("LOGIN_USER");
        if(userModel==null){
            throw new BusinessException(EmBusinessError.USER_NOT_LOGIN);
        }
        Page tmpPage=PageHelper.startPage(page,limit);
        List<BugModel> bugModelList=null;
        List<BugVO> bugVOList=null;
        if(userModel!=null) {
            bugModelList=bugService.listMyBug(userModel.getUsername());
            bugVOList=bugModelList.stream().map(bugModel -> {
                BugVO bugVO = this.convertVOFromModel(bugModel);
                bugVO.setRole(userModel.getRole());
                return bugVO;
            }).collect(Collectors.toList());
        }
        return CommonReturnType.create(bugVOList,tmpPage.getTotal());
    }

    @RequestMapping(value = "/group",method = {RequestMethod.GET})
    @ResponseBody
    public CommonReturnType groupBug(@RequestParam(name = "page") Integer page,
                                     @RequestParam(name = "limit") Integer limit,
                                     HttpSession session){
        UserModel userModel=(UserModel) session.getAttribute("LOGIN_USER");
        Page tmpPage=PageHelper.startPage(page,limit);
        List<BugModel> bugModelList=null;
        List<BugVO> bugVOList=null;
        if(userModel!=null){
            bugModelList=bugService.groupBugByUserName(userModel.getUsername());
            bugVOList=bugModelList.stream().map(bugModel -> {
                BugVO bugVO=this.convertVOFromModel(bugModel);
                bugVO.setUserName(userModel.getUsername());
                bugVO.setRealName(userModel.getRealname());
                return bugVO;
            }).collect(Collectors.toList());
        }
        System.out.println("bugVOList = " + bugVOList);
        return CommonReturnType.create(bugVOList,tmpPage.getTotal());
    }

    @RequestMapping(value = "/confirm",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMAT})
    @ResponseBody
    public CommonReturnType confirmBug(@RequestParam(name = "bugStartDate") String bugStartDate,
                                       @RequestParam(name = "id") Integer bugId,
                                       HttpSession session) throws BusinessException {
        UserModel userModel= (UserModel) session.getAttribute("LOGIN_USER");
        if(userModel==null){
            throw new BusinessException(EmBusinessError.USER_NOT_LOGIN);
        }
        System.out.println("bugId = " + bugId);
        BugModel bugModel=bugService.getBugById(bugId);
        if(bugModel==null){
            throw new BusinessException(EmBusinessError.BUG_NOT_EXIST);
        }
        if(!bugModel.getBugAssginedTo().contains(userModel.getUsername())){
            throw new BusinessException(EmBusinessError.CONFIRM_FAIL);
        }
        bugModel.setBugStartDate(new DateTime(bugStartDate.replace(" ","T")));
        bugModel.setBugStatus("已确认");
        bugService.updateBug(bugModel);
        return CommonReturnType.create(null);
    }
    @RequestMapping(value = "/close",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMAT})
    @ResponseBody
    public CommonReturnType closeBug(@RequestParam(name = "bugEndDate") String bugEndDate,
                                       @RequestParam(name = "id") Integer bugId) throws BusinessException {
        System.out.println("bugId = " + bugId);
        BugModel bugModel=bugService.getBugById(bugId);
        if(bugModel==null){
            throw new BusinessException(EmBusinessError.BUG_NOT_EXIST);
        }
        bugModel.setBugEndDate(new DateTime(bugEndDate.replace(" ","T")));
        bugModel.setBugStatus("已关闭");
        bugService.updateBug(bugModel);
        return CommonReturnType.create(null);
    }
    @RequestMapping(value = "/delete",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMAT})
    @ResponseBody
    public CommonReturnType closeBug(@RequestParam(name = "id") Integer bugId) throws BusinessException {
        System.out.println("bugId = " + bugId);
        BugModel bugModel=bugService.getBugById(bugId);
        if(bugModel==null){
            throw new BusinessException(EmBusinessError.BUG_NOT_EXIST);
        }
        bugService.deleteBugById(bugModel.getId());
        return CommonReturnType.create(null);
    }

    //创建问题单接口
    @RequestMapping(value = "/create",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMAT})
    @ResponseBody
    public CommonReturnType createBug(@RequestParam(name = "bugProjectCode") String bugProjectCode,
                                      @RequestParam(name = "bugPriority") String bugPriority,
                                      @RequestParam(name = "assginedToSelect") String bugAssginedTo,
                                      @RequestParam(name = "bugName") String bugName,
                                      @RequestParam(name = "bugKeyword") String bugKeyword,
                                      @RequestParam(name = "bugEndDate") String bugEndDate,
                                      @RequestParam(name = "bugStep") String bugStep,
                                      @RequestParam(name = "bugCreateDate") String bugCreateDate,
                                      HttpSession session) throws BusinessException {
        UserModel userModel=(UserModel) session.getAttribute("LOGIN_USER");
        if(userModel==null){
            throw new BusinessException(EmBusinessError.USER_NOT_LOGIN);
        }
        BugModel model=bugService.getBugByNameAndProjectCode(bugName,bugProjectCode,bugAssginedTo);
        if(model!=null){
            throw new BusinessException(EmBusinessError.BUG_EXIST);
        }
        BugModel bugModel=new BugModel();
        bugModel.setBugProjectCode(bugProjectCode);
        bugModel.setBugPriority(bugPriority);
        bugModel.setBugAssginedTo(bugAssginedTo);
        bugModel.setBugName(bugName);
        bugModel.setBugKeyword(bugKeyword);
        bugModel.setBugEndDate(new DateTime(bugEndDate.replace(" ","T")));
        bugModel.setBugStep(bugStep);
        bugModel.setBugStatus("未确认");
        bugModel.setBugStartDate(new DateTime(bugCreateDate.replace(" ","T")));
        bugModel.setBugCreateDate(new DateTime(bugCreateDate.replace(" ","T")));
        System.out.println("bugModel.getBugCreateDate() = " + bugModel.getBugCreateDate());
        bugModel.setBugCreater(userModel.getUsername());
        bugService.createBug(bugModel);
        return CommonReturnType.create(null);
    }
    //创建问题单接口
    @RequestMapping(value = "/update",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMAT})
    @ResponseBody
    public CommonReturnType updateBug(@RequestParam(name = "bugProjectCode_edit") String bugProjectCode,
                                      @RequestParam(name = "bugPriority_edit") String bugPriority,
                                      @RequestParam(name = "assginedToSelect_edit") String bugAssginedTo,
                                      @RequestParam(name = "bugName_edit") String bugName,
                                      @RequestParam(name = "id") Integer bugId,
                                      @RequestParam(name = "bugEndDate_edit") String bugEndDate,
                                      @RequestParam(name = "bugStep_edit") String bugStep,
                                      @RequestParam(name = "bugCreateDate_edit") String bugCreateDate,
                                      HttpSession session) throws BusinessException {
        UserModel userModel=(UserModel) session.getAttribute("LOGIN_USER");
        if(userModel==null){
            throw new BusinessException(EmBusinessError.USER_NOT_LOGIN);
        }
        BugModel bugModel=new BugModel();
        bugModel.setId(bugId);
        bugModel.setBugProjectCode(bugProjectCode);
        bugModel.setBugPriority(bugPriority);
        bugModel.setBugAssginedTo(bugAssginedTo);
        bugModel.setBugName(bugName);
        bugModel.setBugEndDate(new DateTime(bugEndDate.replace(" ","T")));
        bugModel.setBugStep(bugStep);
        bugModel.setBugStartDate(new DateTime(bugCreateDate.replace(" ","T")));
        bugModel.setBugCreateDate(new DateTime(bugCreateDate.replace(" ","T")));
        System.out.println("bugModel.getBugCreateDate() = " + bugModel.getBugCreateDate());
        bugService.updateBug(bugModel);
        return CommonReturnType.create(null);
    }

    private BugVO convertVOFromModel(BugModel bugModel){
        if(bugModel==null){
            return null;
        }
        BugVO bugVO=new BugVO();
        BeanUtils.copyProperties(bugModel,bugVO);
        bugVO.setBugStartDate(bugModel.getBugStartDate().toDate());
        bugVO.setBugEndDate(bugModel.getBugEndDate().toDate());
        return bugVO;
    }
}
