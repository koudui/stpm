package stpmproject.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import stpmproject.controller.viewobject.WordVO;
import stpmproject.error.BusinessException;
import stpmproject.error.EmBusinessError;
import stpmproject.response.CommonReturnType;
import stpmproject.service.WordService;
import stpmproject.service.model.UserModel;
import stpmproject.service.model.WordModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/5/26 20:49
 * @software: IntelliJ IDEA
 * @file: WordController
 * @description:
 */
@Controller("word")
@RequestMapping("/word")
@CrossOrigin(allowedHeaders = {"*"},allowCredentials = "true") //springboot前端请求的跨域问题
public class WordController extends BaseController {
    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private WordService wordService;

    @RequestMapping("/get")
    @ResponseBody
    public CommonReturnType getWord(@RequestParam(name = "id") Integer wordId) throws BusinessException {
        WordModel wordModel=wordService.getWordById(wordId);
        if(wordModel==null){
            throw new BusinessException(EmBusinessError.TASK_NOT_EXIST);
        }
        WordVO wordVO=convertVOFromModel(wordModel);
        return CommonReturnType.create(wordVO);
    }

    //获取文档列表,开启分页功能
    @RequestMapping(value = "/list",method = {RequestMethod.GET})
    @ResponseBody
    public CommonReturnType listWord(@RequestParam(name = "page") Integer page,
                                     @RequestParam(name = "limit") Integer limit){
        //开启分页
        Page tmpPage=PageHelper.startPage(page,limit);
        List<WordModel> wordModelList=wordService.listWord();
        List<WordVO> wordVOList=wordModelList.stream().map(wordModel -> {
            WordVO wordVO=this.convertVOFromModel(wordModel);
            return wordVO;
        }).collect(Collectors.toList());
        return CommonReturnType.create(wordVOList,tmpPage.getTotal());
    }

    //创建任务接口
    @RequestMapping(value = "/create",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMAT})
    @ResponseBody
    public CommonReturnType createWord(@RequestParam(name = "wordLib") String wordLib,
                                       @RequestParam(name = "wordTitle") String wordTitle,
                                       @RequestParam(name = "wordKeyword") String wordKeyword,
                                       @RequestParam(name = "wordContent") String wordContent,
                                       @RequestParam(name = "wordType") String wordType,
                                       @RequestParam(name = "wordCreateDate") String wordCreateDate,
                                       HttpSession session) throws BusinessException {
        UserModel userModel=(UserModel) session.getAttribute("LOGIN_USER");
        if(userModel==null){
            throw new BusinessException(EmBusinessError.USER_NOT_LOGIN);
        }
        WordModel model=wordService.getWordByTitle(wordTitle);
        if(model!=null){
            throw new BusinessException(EmBusinessError.WORD_EXIST);
        }
        WordModel wordModel=new WordModel();
        wordModel.setWordLib(wordLib);
        wordModel.setWordTitle(wordTitle);
        wordModel.setWordKeyword(wordKeyword);
        wordModel.setWordContent(wordContent);
        wordModel.setWordType(wordType);
        wordModel.setWordEditDate(new DateTime(wordCreateDate.replace(" ","T")));
        wordModel.setWordCreateDate(new DateTime(wordCreateDate.replace(" ","T")));
        wordModel.setWordCreater(userModel.getUsername());
        wordModel.setWordEditer(userModel.getUsername());
        wordService.createWord(wordModel);
        return CommonReturnType.create(null);
    }

    //更新任务接口
    @RequestMapping(value = "/update",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMAT})
    @ResponseBody
    public CommonReturnType updateWord(@RequestParam(name = "wordLib_edit") String wordLib,
                                       @RequestParam(name = "wordTitle_edit") String wordTitle,
                                       @RequestParam(name = "wordKeyword_edit") String wordKeyword,
                                       @RequestParam(name = "wordContent_edit") String wordContent,
                                       @RequestParam(name = "type_edit") String wordType,
                                       @RequestParam(name = "wordEditDate_edit") String wordCreateDate,
                                       @RequestParam(name = "id") Integer wordId,
                                       HttpSession session) throws BusinessException {
        UserModel userModel=(UserModel) session.getAttribute("LOGIN_USER");
        if(userModel==null){
            throw new BusinessException(EmBusinessError.USER_NOT_LOGIN);
        }
        WordModel model=wordService.getWordById(wordId);
        if(model==null){
            throw new BusinessException(EmBusinessError.WORD_NOT_EXIST);
        }
        WordModel wordModel=new WordModel();
        wordModel.setId(wordId);
        wordModel.setWordLib(wordLib);
        wordModel.setWordTitle(wordTitle);
        wordModel.setWordKeyword(wordKeyword);
        wordModel.setWordContent(wordContent);
        wordModel.setWordType(wordType);
        wordModel.setWordCreater(model.getWordCreater());
        wordModel.setWordCreateDate(model.getWordCreateDate());
        wordModel.setWordEditDate(new DateTime(wordCreateDate.replace(" ","T")));
        wordModel.setWordEditer(userModel.getUsername());
        wordService.updateWord(wordModel);
        return CommonReturnType.create(null);
    }

    @RequestMapping(value = "/delete",method = {RequestMethod.POST},consumes = {CONTENT_TYPE_FORMAT})
    @ResponseBody
    public CommonReturnType deleteWord(@RequestParam(name = "id") Integer wordId) throws BusinessException {
        wordService.deleteWord(wordId);
        return CommonReturnType.create(null);
    }

    private WordVO convertVOFromModel(WordModel wordModel){
        if(wordModel==null){
            return null;
        }
        WordVO wordVO=new WordVO();
        BeanUtils.copyProperties(wordModel,wordVO);
        wordVO.setWordCreateDate(wordModel.getWordCreateDate().toDate());
        wordVO.setWordEditDate(wordModel.getWordEditDate().toDate());
        return wordVO;
    }
}
