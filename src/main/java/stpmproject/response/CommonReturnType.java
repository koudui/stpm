package stpmproject.response;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/4/25 19:15
 * @software: IntelliJ IDEA
 * @file: CommonReturnType
 * @description:
 */
public class CommonReturnType {
    //layui数据表格需要返回code
    private Integer code=0;
    //layui数据表格需要返回count
    private Long count;
    //根据status判断请求是成功还是失败
    private String status;
    //如果status为success，则返回给前端需要的json数据
    //如果status为fail，则返回通用的错误码格式
    private Object data;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    //定义一个通用的构造方法
    public static CommonReturnType create(Object result){
        return CommonReturnType.create(result,"success");
    }
    public static CommonReturnType create(Object result,String status){
        CommonReturnType type=new CommonReturnType();
        type.setData(result);
        type.setStatus(status);
        return type;
    }
    public static CommonReturnType create(Object result,Long count){
        return CommonReturnType.create(result,count,"success");
    }
    public static CommonReturnType create(Object result,Long count,String status){
        CommonReturnType type=new CommonReturnType();
        if(status.equals("success")){
            type.setCode(0);
        }else{
            type.setCode(-1);
        }
        type.setStatus(status);
        type.setData(result);
        type.setCount(count);
        return type;
    }
}
