package stpmproject.error;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/4/25 19:24
 * @software: IntelliJ IDEA
 * @file: EmBusinessError
 * @description:
 */
public enum EmBusinessError implements CommonError {
    //通用错误类型10001
    PARAMETER_VALIDATION_ERROR(10001,"参数不合法"),
    UNKOWN_ERROR(10002,"未知错误"),
    UPDATE_FAIL(10003,"更新失败"),
    DELETA_FAIL(10004,"删除失败"),
    START_FAIL(10005,"你没有权限开始"),
    FINISH_FAIL(10006,"你没有权限完成"),
    DELETE_FAIL_2(10007,"你没有权限删除"),
    CONFIRM_FAIL(10008,"你没有权限确认"),
    //用户信息相关错误码定义20001
    USER_NOT_EXIST(20001,"用户不存在"),
    USER_LOGIN_FAIL(20002,"用户名或密码错误"),
    USER_NOT_LOGIN(20003,"用户未登录"),
    USERNAME_PASSWORD_ISEMPTY(20004,"用户名或密码为空"),
    TELPHONE_NOT_EXIST(20005,"手机号不存在"),
    TELPHONE_EXITS(20006,"手机号已经被注册"),
    USER_EXIST(20007,"用户名已经被注册"),
    USER_UPDATE_FAIL(20008,"用户更新失败"),
    USER_DELETE_FAIL(20009,"用户删除失败"),
    PASSWORD_ERROR(20010,"原密码错误"),
    //项目类错误
    PROJECT_EXIST(30001,"项目代号已经存在"),
    PROJECT_NOT_EXIST(30002,"该项目不存在"),
    PROJECT_UPDATE_FAIL(30003,"项目更新失败"),
    PROJECT_DELETE_FAIL(30004,"项目删除失败"),
    //团队类错误
    TEAM_NOT_EXIST(40001,"团队不存在"),
    TEAM_EXIST(40002,"一个项目不能有两个相同名的团队"),
    //任务类错误
    TASK_NOT_EXIST(50001,"任务不存在"),
    TASK_EXIST(50002,"同一项目不能存在指派给同一团队的两个任务"),
    //问题单类错误
    BUG_NOT_EXIST(60001,"问题单不存在"),
    BUG_EXIST(60001,"同一个项目中指派给同一人的只能有一个相同问题单"),
    //文档类错误
    WORD_NOT_EXIST(70001,"文档不存在"),
    WORD_EXIST(70001,"文档已经存在存在"),
    //todo类错误
    TODO_NOT_EXIST(80001,"代办事项不存在"),
    TODO_EXIST(80001,"代办事项已经存在")
    ;

    private int errCode;
    private String errMsg;
    EmBusinessError(int errCode,String errMsg){
        this.errCode=errCode;
        this.errMsg=errMsg;
    }

    @Override
    public int getErrCode() {
        return this.errCode;
    }

    @Override
    public String getErrMsg() {
        return this.errMsg;
    }

    @Override
    public CommonError setErrMsg(String errMsg) {
        this.errMsg=errMsg;
        return this;
    }
}
