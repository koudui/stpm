package stpmproject.error;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/4/25 19:28
 * @software: IntelliJ IDEA
 * @file: BusinessException
 * @description:
 */
//异常类的实现
public class BusinessException extends Exception implements CommonError {
    private CommonError commonError;//定义接口
    //接收枚举类EmBusinessError作为参数
    public BusinessException(CommonError commonError){
        super();//调用Exception父类的构造方法
        this.commonError=commonError;
    }
    //重载构造方法，加入errMsg作为参数
    public BusinessException(CommonError commonError,String errMsg){
        super();
        this.commonError=commonError;
        this.commonError.setErrMsg(errMsg);
    }
    @Override
    public int getErrCode() {
        return this.commonError.getErrCode();
    }

    @Override
    public String getErrMsg() {
        return this.commonError.getErrMsg();
    }

    @Override
    public CommonError setErrMsg(String errMsg) {
        this.commonError.setErrMsg(errMsg);
        return this;
    }
}
