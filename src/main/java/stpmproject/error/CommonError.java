package stpmproject.error;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/4/25 19:22
 * @software: IntelliJ IDEA
 * @file: CommonError
 * @description:
 */
public interface CommonError {
    int getErrCode();
    String getErrMsg();
    CommonError setErrMsg(String errMsg);
}
