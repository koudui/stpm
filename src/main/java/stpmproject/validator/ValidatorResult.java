package stpmproject.validator;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;

/**
 * encoding: utf-8
 *
 * @Author: kou dui
 * @Date: 2019/4/25 19:35
 * @software: IntelliJ IDEA
 * @file: ValidatorResult
 * @description:
 */
public class ValidatorResult {
    //是否有错误信息
    private Boolean hasErr=false;
    //返回具体的错误信息
    private HashMap<String,String> errMap=new HashMap<>();

    public Boolean getHasErr() {
        return hasErr;
    }

    public void setHasErr(Boolean hasErr) {
        this.hasErr = hasErr;
    }

    public HashMap<String, String> getErrMap() {
        return errMap;
    }

    public void setErrMap(HashMap<String, String> errMap) {
        this.errMap = errMap;
    }
    //用于返回错误结果
    public String getErrMsg(){
        return StringUtils.join(errMap.values().toArray(),",");
    }
}
